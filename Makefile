TEX=main.tex
BIB=$(basename $(TEX))

all:
	pdflatex $(TEX)
	pdflatex $(TEX)

.PHONY: bib
bib:
	pdflatex $(TEX)
	bibtex $(BIB)
	pdflatex $(TEX)
	pdflatex $(TEX)


clean:
	rm -f *.pdf *.aux *.bbl *.log *.dvi *.ps *.blg *.bcf *.out *-blx.bib *.run.xml *.idx *.ilg *.ind *.lof *.lot *.toc

