\chapter{Elements of Functional Analysis} \label{chap:fun-anal}
In this chapter, we review the relevant background from functional analysis used
throughout this dissertation. We will mostly be interested in function spaces of
functions mapping $\R^d \to \R$ (or $\C$) and functions mapping $\cyl \to \R$
(or $\C$), where
\[
  \Sph^{d-1} \coloneqq \curly{\vec{x} \in \R^d \st \norm{\vec{x}}_2 = 1}
\]
denotes the Euclidean sphere in $\R^d$. We use the term function space to mean a
topological vector space whose elements are functions.

\section{Spaces of Functions, Measures, and Distributions} \label{sec:spaces}
\paragraph{$L^p$ spaces.}
For $1 \leq p \leq \infty$, let $L^p(\R^d)$ denote the Lebesgue space on $\R^d$,
which is a Banach space when equipped with the norm
\[
  \norm{f}_{L^p} \coloneqq \begin{cases}
    \ds\paren{\int_{\R^d} \abs{f(\vec{x})}^p \dd \vec{x}}^{1/p}, & 1 \leq p <
    \infty, \\
    \ds\esssup_{\vec{x} \in \R^d} \: \abs{f(\vec{x})}, & p = \infty.
  \end{cases}
\]
The space $L^p(\cyl)$ is defined analogously, except that the underlying measure
is the product measure of the surface measure on $\Sph^{d-1}$ and the univariate
Lebesgue measure on $\R$. The space $L^p$ is a Banach space
for $1 \leq p \leq \infty$ and a Hilbert space if and only if $p = 2$.

\paragraph{The spaces $\Sch$ and $\Sch'$.}
Let $\Sch(\R^d)$ denote the Schwartz space of smooth and rapidly decaying test
functions on $\R^d$. These are functions $\varphi \in C^\infty(\R^d)$ such that
\[
  p_{\vec{\alpha}, \vec{\beta}}(\varphi) \coloneqq \sup_{\vec{x} \in \R^d}
  \abs{\vec{x}^\vec{\alpha} (\partial^\vec{\beta} \varphi)(\vec{x})} <
  \infty, \quad \vec{\alpha}, \vec{\beta} \in \N_0^d,
\]
where $\vec{x}^\vec{\alpha} = x_1^{\alpha_1} \cdots x_d^{\alpha_d}$ and
$\partial^\vec{\beta} = \partial_{x_1}^{\beta_1} \cdots
\partial_{x_d}^{\beta_d}$ is the usual multi-index notation, and
$C^\infty(\R^d)$ denotes the space of infinitely differentiable functions on
$\R^d$. We endow $\Sch(\R^d)$ with the topology induced by by the family of
seminorms $\curly{p_{\vec{\alpha}, \vec{\beta}}}_{\vec{\alpha}, \vec{\beta} \in
\N_0^d}$, making it a Fr\'echet space~\citep[Chapter~7]{RudinFA}. Let
$\Sch(\cyl)$ denote the Schwartz space of smooth and rapidly decaying test
functions on $\cyl$, defined as $\Sch(\cyl) \coloneqq C^\infty(\Sph^{d-1})
\,\hat{\otimes}\, \Sch(\R)$, where $\hat{\otimes}$ denotes the topological
tensor product~\citep[Chapter~43]{TrevesTVS}.

The continuous dual of $\Sch(\R^d)$ (resp. $\Sch(\cyl)$) is the space of
tempered distributions on $\R^d$ (resp. $\cyl$), denoted $\Sch'(\R^d)$ (resp.
$\Sch'(\cyl)$). This is the space of continuous linear functionals on
$\Sch(\R^d)$ (resp. $\Sch(\cyl)$)~\citep[Chapter~7]{RudinFA}.  In particular, a
tempered distribution $u \in \Sch'(\R^d)$ defines a continuous linear functional
on the space of Schwartz functions $\Sch(\R^d)$ via $u: \varphi \mapsto \ang{u,
\varphi}$, where $\ang{\dummy, \dummy}$ denotes the \emph{duality pairing}
between $\Sch(\R^d)$ and $\Sch'(\R^d)$. The duality pairing between $\psi \in
\Sch(\cyl)$ and $v \in \Sch'(\cyl)$ will be denoted by $\sq{v, \psi}$. We abuse
notation and also let $\ang{\dummy, \dummy}$ (resp.  $\sq{\dummy, \dummy}$)
denote the pairing between any dual pair of spaces on $\R^d$ (resp. $\cyl$),
where the exact pairing will be clear from context. We also use $\ang{\dummy,
\dummy}$ and $\sq{\dummy, \dummy}$ to denote the corresponding $L^2$-inner
products.

\paragraph{The spaces $C_0$ and $\M$.}
Let $C_0(\R^d)$ denote the space of continuous functions on $\R^d$ vanishing at
infinity. This space is a Banach space when equipped with the $L^\infty$-norm.
By the Riesz--Markov--Kakutani representation theorem, the continuous dual of
$C_0(\R^d)$ is the space $\M(\R^d)$ of finite Radon measures on $\R^d$, which is
a Banach space when equipped with the norm
\[
  \norm{u}_\M \coloneqq \sup_{\substack{\varphi \in C_0(\R^d) \\
  \norm{\varphi}_{L^\infty} = 1}} \ang{u, \varphi},
\]
where $\ang{\dummy, \dummy}$ denotes the pairing between $C_0(\R^d)$ and
$\M(\R^d)$. It is well-known that $C_0(\R^d) = \cl{(\Sch(\R^d),
\norm{\dummy}_{L^\infty})}$ (the closure of $\Sch(\R^d)$ with respect to
$\norm{\dummy}_{L^\infty}$). Therefore, we can alternatively define $\M(\R^d) =
\paren{C_0(\R^d)}'$ as
\[
  \M(\R^d) = \curly{u \in \Sch'(\R^d) \st \norm{u}_{\M} =
  \sup_{\substack{\varphi \in \Sch(\R^d) \\
  \norm{\varphi}_{L^\infty} = 1}} \ang{u, \varphi} < \infty}.
\]
The $\M$-norm is exactly the \emph{total variation norm} in the sense of
measures~\citep[Chapter~7]{FollandRA}.  The definition in the above display allows us to view $\M(\R^d)$ as a
subspace of $\Sch'(\R^d)$.  The Banach space $(\M(\R^d), \norm{\dummy}_\M)$ can
be viewed as a ``generalization'' of the Banach space $(L^1(\R^d),
\norm{\dummy}_{L^1})$.  Indeed, this is due to the following three properties:
\begin{enumerate}
  \item $L^1(\R^d) \subset \M(\R^d)$, where the containment is strict;
  \item The shifted Dirac impulse $\delta(\dummy - \vec{x}_0)$, $\vec{x}_0 \in
    \R^d$, is not contained in $L^1(\R^d)$, but $\delta(\dummy - \vec{x}_0) \in
    \M(\R^d)$ with $\norm{\delta(\dummy - \vec{x}_0)}_\M = 1$;
  \item For every $f \in L^1(\R^d)$, we have that $\norm{f}_{L^1} =
    \norm{f}_\M$.
\end{enumerate}
Working in this formalism allows us to work rigorously with (tempered)
distributions such as the Dirac impulse, which is often overlooked in
engineering textbooks~(see, e.g.,~\citet{FeichtingerLTI,UnserBIBO} for more
details). The spaces $C_0(\cyl)$ and $\M(\cyl)$ are defined analogously, where
the same properties hold.

\section{Linear Operators}
We will primarily be interested in linear operators mapping between functions
mapping $\R^d \to \R$ (or $\C$) and functions mapping $\R^d \to \R$ (or $\C$)
and linear operators mapping from functions mapping functions mapping $\cyl \to
\R$ (or $\C$) and functions mapping $\R^d \to \R$ (or $\C$). In particular, we
will want to apply these operators to functions that are tempered distributions.

\begin{definition} \label[defn]{defn:adjoint-Rd}
  Let $\LOp: \Sch(\R^d) \to \Sch'(\R^d)$ be a continuous linear operator. The
  \emph{adjoint} of $\LOp$ is the unique continuous linear operator $\LOp^*:
  \Sch(\R^d) \to \Sch'(\R^d)$ such that
  \[
    \ang{\LOp\curly{\varphi}, \phi} = \ang{\LOp^*\curly{\phi}, \varphi},
  \]
  for all $\varphi, \phi \in \Sch(\R^d)$, where the duality pairing $\ang{\dummy,
  \dummy}$ is the pairing between $\Sch(\R^d)$ and $\Sch'(\R^d)$.
\end{definition}

\begin{definition} \label[defn]{defn:adjoint-cyl}
  Let $\TOp: \Sch(\R^d) \to \Sch'(\cyl)$ be a continuous linear operator. The
  \emph{adjoint} of $\TOp$ is the unique continuous linear operator $\TOp^*:
  \Sch(\cyl) \to \Sch'(\R^d)$ such that
  \[
    \sq{\TOp\curly{\varphi}, \psi} = \ang{\TOp^*\curly{\psi}, \varphi},
  \]
  for all $\varphi\in \Sch(\R^d)$ and $\psi \in \Sch(\cyl)$, where the duality pairing $\ang{\dummy,
  \dummy}$ (resp. $\sq{\dummy, \dummy}$) is the pairing between $\Sch(\R^d)$ and
  $\Sch'(\R^d)$ (resp. $\Sch(\cyl)$ and $\Sch'(\cyl)$).
\end{definition}

\begin{remark}
  The definition of the adjoint of an operator mapping $\Sch(\cyl) \to
  \Sch'(\R^d)$ is analogous to \cref{defn:adjoint-cyl}.
\end{remark}

One can quickly verify that for both \cref{defn:adjoint-Rd,defn:adjoint-cyl},
the double adjoint of an operator is itself. Indeed, we illustrate this
explicitly for a continuous linear operator $\LOp: \Sch(\R^d) \to \Sch'(\R^d)$.
By \cref{defn:adjoint-Rd} we have for all $\varphi, \phi \in \Sch(\R^d)$ the
following two equalities:
\begin{enumerate*}[label=(\roman*)]
  \item $\ang{\LOp^*\curly{\phi}, \varphi} = \ang{\LOp\curly{\varphi}, \phi}$;
  \item $\ang{\LOp^*\curly{\phi}, \varphi} = \ang{\LOp^{**}\curly{\varphi},
    \phi}$.
\end{enumerate*}
Subtracting these two equalities yields $\ang{\LOp\curly{\varphi}, \phi} -
\ang{\LOp^{**}\curly{\varphi}, \phi} = 0$, i.e., for all $\varphi, \phi \in
\Sch(\R^d)$, $\ang{\LOp\curly{\varphi} - \LOp^{**}\curly{\varphi}, \phi} = 0$.
Therefore, $\LOp^{**}\curly{\varphi} = \LOp\curly{\varphi}$ for all $\varphi \in
\Sch(\R^d)$.

\paragraph{The Schwartz kernel theorem.}
Linear operators can be completely characterized by their (Schwartz) kernel.
This is summarized by the Schwartz kernel theorem.

\begin{theorem}[Schwartz kernel theorem] \label{thm:Schwartz-kernel-theorem-Rd}
  Let $\LOp: \Sch(\R^d) \to \Sch'(\R^d)$ be a continuous linear operator. Then,
  there exists a unique tempered distribution $h \in \Sch'(\R^d \times \R^d)$
  such that
  \begin{equation}
    \ang{\LOp\curly{\varphi}, \phi} = \paren{h, \varphi \otimes \phi},
    \label{eq:schwartz-kernel-thm-Rd}
  \end{equation}
  for all $\varphi, \phi \in \Sch(\R^d)$, where $\paren{\dummy, \dummy}$ denotes
  the pairing between $\Sch(\R^d \times \R^d)$ and $\Sch'(\R^d \times \R^d)$ and
  $(\varphi \otimes \phi)(\vec{x}, \vec{y}) = \varphi(\vec{x}) \phi(\vec{y})$
  is the tensor product between $\varphi$ and $\phi$.
\end{theorem}
\begin{remark}
  When $\LOp\curly{\varphi}$ and $h$ are both locally integrable functions,
  \cref{eq:schwartz-kernel-thm-Rd} can be rewritten as
  \[
    \LOp\curly{\varphi}(\vec{x}) = \int_{\R^d} h(\vec{x}, \vec{y}) \varphi(\vec{y}) \dd
    \vec{y}.
  \]
  When $\LOp\curly{\varphi}$ and $h$ are not both locally integral functions, we
  occasionally abuse notation and write $\LOp\curly{\varphi}$ as in the
  above display.
\end{remark}
\begin{remark}
  When the operator $\LOp$ is shift-invariant, its kernel
  $h(\vec{x}, \vec{y})$ satisfies $h(\vec{x}, \vec{y}) = h(\vec{x} - \vec{y})$
  for some $h \in \Sch'(\R^d)$.
  Then $\LOp$ is a convolution operator with $\LOp\curly{\varphi} = h *
  \varphi$. In this case, $h = \LOp\curly{\delta}$ is known as the \emph{impulse
  response} of $\LOp$.
\end{remark}

The Schwartz kernel theorem is intimately linked to the \emph{nuclearity} of
$\Sch(\R^d)$~\citep[Chapters~50~and~51]{TrevesTVS}. While the form of the
theorem in \cref{thm:Schwartz-kernel-theorem-Rd} can be proved using elementary
techniques~\citep[see, e.g.,][Theorem~5]{SimonDistributions}, the result
actually holds more generally on locally convex nuclear spaces via more advanced
techniques~\citep{GrothendieckNuclear}. For example, consider the following
variant of the Schwartz kernel theorem.

\begin{theorem}
  Let $\TOp: \Sch(\R^d) \to \Sch'(\cyl)$ be a continuous linear operator. Then,
  there exists a unique tempered distribution $h \in \Sch'(\R^d \times \cyl)$
  such that
  \[
    \sq{\TOp\curly{\varphi}, \psi} = \paren{h, \varphi \otimes \psi},
  \]
  for all $\varphi \in \Sch(\R^d)$ and $\psi \in \Sch(\cyl)$, where $\paren{\dummy, \dummy}$ denotes
  the pairing between $\Sch(\R^d \times \cyl)$ and $\Sch'(\R^d \times \cyl)$ and
  $(\varphi \otimes \psi)(\vec{x}, \vec{z}) = \varphi(\vec{x}) \psi(\vec{z})$
  is the tensor product between $\varphi$ and $\phi$, where $\vec{x} \in \R^d$
  and $\vec{z} \in \cyl$.
\end{theorem}

\begin{remark}
  Another variant of the Schwartz kernel theorem holds for continuous linear
  operators mapping $\Sch(\cyl) \to \Sch'(\R^d)$.
\end{remark}

\paragraph{Extension of operators by duality.}
Continuous linear operators that map $\Sch(\R^d) \to \Sch(\R^d)$ can be extended
to map $\Sch'(\R^d) \to \Sch'(\R^d)$. Indeed, suppose that $\LOp: \Sch(\R^d) \to
\Sch(\R^d)$ and $\LOp^*: \Sch(\R^d) \to \Sch(\R^d)$ are both continuous linear
operators. Then, for $u \in \Sch'(\R^d)$, we \emph{define} $\LOp\curly{u}$ as
the tempered distribution such that
\[
  \ang{\LOp\curly{u}, \varphi} = \ang{u, \LOp^*\curly{\varphi}}
\]
for all $\varphi \in \Sch(\R^d)$. This technique can be applied more generally,
e.g., for continuous linear operators mapping $\Sch(\R^d) \to \Sch(\cyl)$ and
continuous linear operators mapping $\Sch(\cyl) \to \Sch(\R^d)$.

\section{Two Topologies of a Dual Banach Space}
Given a Banach space $(\X, \norm{\dummy}_{\X})$, we will be interested in two
topologies for its continuous dual $\X'$, which is a Banach space when equipped
with the (dual) norm
\[
  \norm{u}_{\X'} \coloneqq \sup_{\substack{v \in \X \\ \norm{v}_{\X} = 1}}
  \ang{u, v},
\]
where $\ang{\dummy, \dummy}$ is the duality pairing between $\X$ and $\X'$.

\begin{definition} \label[defn]{defn:strong-topology}
  A sequence $\curly{u_n}_{n=1}^\infty\subset \X'$ is said to converge to $f$ in
  the \emph{strong topology} if
  \[
    \lim_{n \to \infty} \norm{u_n - u}_{\X'} = 0.
  \]
  In other words, the strong topology of $\X'$ is the topology induced by the
  norm $\norm{\dummy}_{\X'}$ and is the usual topology implicitly assumed when
  working with the Banach space $\X'$.
\end{definition}
\begin{definition} \label[defn]{defn:weak*-topology}
  A sequence $\curly{u_n}_{n=1}^\infty\subset \X'$ is said to converge to $u$ in
  the weak$^*$ topology if
  \[
    \lim_{n \to \infty} \ang{u_n - u, v} = 0,
  \]
  for all $v \in \X$.
\end{definition}

\begin{remark}
  The weak$^*$ topology is \emph{coarser} than the strong topology of $\X'$.
\end{remark}

We will be interested in the continuity of linear functionals on dual Banach
spaces with respect to the two topologies in
\cref{defn:strong-topology,defn:weak*-topology}. In particular, the space of all
linear functionals on $\X'$ which are continuous with respect to the strong
topology is (by definition) its continuous dual $\X''$. We refer to these
linear functionals as being continuous on $\X'$.

\begin{proposition}[{\citet[Theorem~IV.20, pg. 114]{ReedMP}}] \label[prop]{prop:weak*-continuous-predual}
  The space of weak$^*$ continuous linear functionals on
  $\X'$ is the space $\X$.
\end{proposition}

From \cref{prop:weak*-continuous-predual} we see that weak$^*$ continuity is
actually a stronger notion of continuity than the standard notion. Indeed, this
is due to the fact that a Banach space $\X$ is isometrically isomorphic to a
closed subspace of its bidual $\X''$~\citep{RudinFA}. In particular, we can view
$\X \cembed \X''$ via the canonical embedding of a Banach space into its bidual.
When $\X$ is a reflexive Banach space, the inclusion is actually an equality,
i.e., $\X = \X''$, and therefore the space of continuous linear functionals on
$\X'$ is the same as the space of weak$^*$ continuous linear functionals on
$\X'$. On the other hand, when $\X$ is a non-reflexive Banach space, the
inclusion $\X \subset \X''$ is strict. In this dissertation, we will mostly be
interested in non-reflexive spaces, e.g., $\X' = \paren{C_0(\R^d)}' = \M(\R^d)$.

\paragraph{The Banach--Alaoglu theorem.} It is well-known that closed balls are
not compact in infinite-dimensional spaces with respect to the topology induced
by the norm, i.e., the strong topology of a dual space. The utility of working
with the weak$^*$ topology is that closed balls are compact in the weak$^*$
topology by the Banach--Alaoglu theorem~\citep[Chapter~3]{RudinFA}.
\begin{theorem}[Banach--Alaoglu theorem]
  The closed unit ball
  \[
    B \coloneqq \curly{f \in \X' \st \norm{f}_{\X'} \leq 1}
  \]
  is weak$^*$ compact.
\end{theorem}
\begin{remark}
  This result allows us to use compactness arguments to prove that solutions
  exist to certain variational problems.
\end{remark}

\section{Direct-Sum Decompositions and Projectors}
There are two ways of working with direct-sums of topological vector spaces:
\begin{enumerate*}[label=(\roman*)]
  \item explicitly, via projectors;
  \item abstractly, via quotient spaces and equivalence classes.
\end{enumerate*}
These two methods are equivalent whenever one can identify abstract quotient
space as a \emph{concrete subspace} of the original space. In other words, by
selecting a concrete representer from each coset (which is an equivalence
class). In this dissertation, we will work with direct-sums explicitly via
projectors rather than abstractly.

Let $\X$ be a topological vector space. A continuous linear operator $\P: \X \to
\X$ with the property that $\P^2 = \P$ on $\X$ is called a \emph{projection
operator} or a \emph{projector}~\citep[pg.~140]{DunfordLinearOps}. When $\X$ is
a Fr\'echet space, then $\U \coloneqq \P\paren{\X}$ is a closed subspace of
$\X$. In this case, $\P$ is the projector of $\X$ onto $\U$, i.e., $\P =
\Proj_\U$, and we have the direct-sum decomposition $\X = \U \oplus \cV$, where
$\cV$ is the null space of $\P$. Said differently, the projector of $\X$ onto
$\cV$ is the \emph{complementary projector} of $\P$, i.e., $\Proj_\cV = \Id - \P$,
where $\Id$ is the identity operator. The space $\U = \P\paren{\X}$ is also a
topological vector space with the topology induced by the topology of $\X$. Let
$(\X, \X')$ be a dual pair of topological spaces, the $(\U, \U')$ is also a dual
pair of topological spaces where $\U' = \P^*\paren{\X'}$, where $\P^*: \X' \to
\X'$ is the dual (adjoint) projector.


\section{The Fourier, Hilbert, and Radon Transforms} \label{sec:transforms}
\paragraph{The Fourier transform.}
The Fourier transform of $\varphi \in \Sch(\R^d)$ is given by
\[
  \hat{\varphi}(\vec{\omega}) = \Fourier{\varphi}(\vec{\omega}) = \int_{\R^d}
  \varphi(\vec{x}) e^{-\imag \vec{\omega}^\T\vec{x}} \dd\vec{x}, \quad
  \vec{\omega} \in \R^d,
\]
where $\imag^2 = -1$.  The Fourier transform $\FourierOp: \Sch(\R^d) \to
\Sch(\R^d)$ is a continuous linear bijection whose inverse $\FourierOp^{-1}:
\Sch(\R^d) \to \Sch(\R^d)$ is also continuous~\citep[Chapter~7]{RudinFA}. The
inverse Fourier transform of $\hat{\varphi} \in \Sch(\R^d)$ is given by
\begin{equation}
  \InvFourier{\hat{\varphi}}(\vec{x}) = \frac{1}{(2\pi)^d} \int_{\R^d}
  \hat{\varphi}(\vec{\omega}) e^{\imag\vec{\omega}^\T\vec{x}} \dd\vec{\omega},
  \quad \vec{x} \in \R^d.
  \label{eq:Fourier-inversion}
\end{equation}
We can extend $\FourierOp$ and $\FourierOp^{-1}$ to act on $\Sch'(\R^d)$ by
duality.

An important property of the Fourier transform is Plancherel's theorem, which
states that $\FourierOp: L^2(\R^d) \to L^2(\R^d)$ is an isometry. More
specifically, given $\varphi \in \Sch(\R^d)$, we have the equality
\[
  (2\pi)^d \norm*{\varphi}_{L^2}^2 = \norm*{\hat{\varphi}}_{L^2}^2,
\]
and the operator $\FourierOp: \Sch(\R^d) \to \Sch(\R^d)$ admits a
unique extension $\FourierOp: L^2(\R^d) \to L^2(\R^d)$ since $L^2(\R^d) =
\cl{(\Sch(\R^d), \norm{\dummy}_{L^2})}$.

\paragraph{The Hilbert transform.}
The Hilbert transform of $\varphi \in \Sch(\R)$ is given by
\[
  \Hilbert{\varphi}(x) = \frac{1}{\pi} \pv \int_{\R} \frac{f(x - y)}{y} \dd y
  \coloneqq \frac{1}{\pi} \lim_{\varepsilon \to 0} \int_{\abs{y} > \varepsilon}
  \frac{f(x - y)}{y} \dd y, \quad x \in \R,
\]
where $\pv$ denotes that the integral is understood in the Cauchy principle
value sense, as defined above. Although the Hilbert transform of a Schwartz
function is not a Schwartz function, one may quickly verify that $\HilbertOp$
maps $\Sch(\R)$ to $L^2(\R)$. In particular, for $\varphi \in \Sch(\R)$, the
Hilbert transform satisfies
\begin{equation}
  \hat{\HilbertOp{\varphi}}(\omega) = -\imag \sgn(\omega) \hat{\varphi}(\omega),
  \label{eq:Hilbert-Fourier-defn}
\end{equation}
where $\omega \mapsto -\imag \sgn(\omega)$ is the \emph{frequency response} (or
Fourier symbol/multiplier) of $\HilbertOp$, where the Fourier transform in the
above display is understood as the Fourier transform of an $L^2(\R)$ function,
i.e., defined via density by Plancheral's theorem. From
\cref{eq:Hilbert-Fourier-defn}, we see that the Hilbert transform is
skew-adjoint on $L^2(\R)$, i.e., $\HilbertOp^* = -\HilbertOp$; in particular,
$\HilbertOp$ is unitary.  Although the Hilbert transform \emph{cannot} be
extended to $\Sch'(\R^d)$ by duality, it can be extended to a large class of
distributions, which suffices for our
purposes~\citep[Chapter~3]{PandeyHilbertTransform}.

\paragraph{The Radon transform.} The Radon transform of $\varphi \in \Sch(\R^d)$
is given by
\[
  \Radon{\varphi}(\vec{\alpha}, t) = \int_{\R^d} \varphi(\vec{x})
  \delta(\vec{\alpha}^\T\vec{x} - t) \dd\vec{x}, \quad (\vec{\alpha}, t) \in
  \cyl,
\]
where $\delta$ is the univariate Dirac impulse. The Radon domain is the
hypercylinder $\cyl$, with a \emph{direction} variable $\vec{\alpha} \in
\Sph^{d-1}$ and an \emph{offset} variable $t \in \R$. Note that the Radon
transform of $\varphi$ evaluated at $(\vec{\alpha}, t)$ is precisely the
integral of $\varphi$ over the hyperplane given by 
\[
  P_{(\vec{\alpha}, t)} \coloneqq \curly{\vec{x} \in \R^d \st
  \vec{\alpha}^\T\vec{x} = t}.
\]
Since $P_{(\vec{\alpha}, t)} = P_{(-\vec{\alpha}, -t)}$, we see that the Radon
transform is always an even function. The Radon transform maps $\Sch(\R^d)$ to a
subspace of $\Sch(\cyl)$. This subspace is characterized by the following range
theorem for the Radon transform.

\begin{theorem}[{\citet[Theorem~2.1]{LudwigRadon}}] \label{thm:Radon-range}
  A function $\psi$ is the Radon transform of a function $\varphi
  \in \Sch(\R^d)$ if and only if
  \begin{enumerate}[leftmargin=*]
    \item $\psi \in \Sch(\cyl)$;
    \item $\psi$ is even, i.e., $\psi(\vec{\alpha}, t) = \psi(-\vec{\alpha},
      -t)$;
    \item $\Psi_k(\vec{\alpha}) \coloneqq \ds \int_\R \psi(\vec{\alpha}, t) t^k
      \dd t$ is a homogeneous polynomial (in $\vec{\alpha}$) for all $k \in
      \mathbb{N}_0$. \label{item:Radon-moment-conditions}
  \end{enumerate}
\end{theorem}
In other words, the range of the Radon transform $\SchRad \coloneqq \RadonOp\paren{\Sch(\R^d)}$ is
the subspace of $\Sch(\cyl)$ satisfying the properties in
\cref{thm:Radon-range}. The conditions in \cref{item:Radon-moment-conditions}
are often referred to as the moment (or Cavalieri) conditions of the Radon
transform.

The Radon transform is invertible on $\Sch(\R^d)$ via the so-called
\emph{filtered backprojection operator}. The Radon transform itself is sometimes
referred to as the \emph{projection operator} since given $\varphi \in
\Sch(\R^d)$, for a fixed direction $\vec{\alpha}_0 \in \Sph^{d-1}$, the function
$\Radon{\varphi}(\vec{\alpha}_0, \dummy)$ is a univariate function which
corresponds to the projection\footnote{Technically speaking, this is not truly
projection.} of $\varphi$ in the direction specified by $\vec{\alpha}_0$. The
adjoint of the Radon transform is the so-called \emph{backprojection operator}
and is given by
\[
  \DualRadon{\psi}(\vec{x}) = \int_{\Sph^{d-1}} \psi(\vec{\alpha},
  \vec{\alpha}^\T\vec{x}) \dd \sigma(\vec{\alpha}),
\]
for sufficiently nice functions $\psi: \cyl \to \R$, where $\sigma$ denotes
the surface measure on $\Sph^{d-1}$. Given a function $\varphi \in \Sch(\R^d)$,
a calculation shows that
\[
  \reallywidehat{\RadonOp^*\RadonOp \varphi}(\vec{\omega}) =
  \frac{\hat{\varphi}(\vec{\omega})}{c_d \norm{\vec{\omega}}_2^{d-1}},
\]
where $c_d \coloneqq 1 / (2(2\pi)^{d-1})$. In other words, the result of applying the
projection followed by the backprojection to $\varphi$ results in a ``blurring''
(attenuation of high frequencies) of $\varphi$. We can ``deblur'' the projected
backprojection by applying a \emph{ramp filter} to amplify high frequencies. The
exact (spatial domain) filter is given by $\paren{\RadonOp^*\RadonOp}^{-1}$
whose frequency response is
\begin{equation}
  \vec{\omega} \mapsto c_d \norm{\vec{\omega}}_2^{d-1}.
  \label{eq:Laplacian-frequency-response}
\end{equation}
This is realized by the operator
\begin{equation}
  \paren{\RadonOp^*\RadonOp}^{-1} = c_d (-\Delta)^{\frac{d-1}{2}},
  \label{eq:RadjRinv}
\end{equation}
where $\Delta = \partial_{x_1}^2 + \cdots + \partial_{x_d}^2$ is the
$d$-dimensional Laplacian operator.  Therefore, for $\varphi \in \Sch(\R^d)$, we
have the following inversion formula\footnote{This inversion formula also holds
for $\varphi \in L^1(\R^d)$.} for the Radon transform:
\[
  c_d (-\Delta)^{\frac{d-1}{2}} \RadonOp^* \RadonOp \varphi = \varphi.
\]
The operator $c_d (-\Delta)^{\frac{d-1}{2}} \RadonOp^*$ is known as the
\emph{filtered backprojection operator}.

Since the frequency response in \cref{eq:Laplacian-frequency-response} is a
radial function, by the \emph{intertwining properties} of the Radon
transform~\citep[Lemma~2.1]{HelgasonIntegralGeometry}, the filtering can also be
carried out in the Radon domain via the filter
\[
  \KOp^{d-1} \coloneqq c_d (-\partial_t^2)^{\frac{d-1}{2}} = \begin{cases}
    c_d (-1)^{\frac{d-1}{2}} \partial_t^{d-1}, & \text{$d$ is odd} \\
    c_d (-1)^{\frac{d-2}{2}} \HilbertOp_t \partial_t^{d-1}, & \text{$d$ is even},
  \end{cases}
\]
where $\HilbertOp_t$ denotes the Hilbert transform with respect to the $t$
variable. The frequency response of this operator is
\[
  \hat{K}^{d-1}(\omega) = c_d \abs{\omega}^{d-1},
\]
where the Fourier transform is the univariate Fourier transform with respect to
$t \to \omega$. Due to the Hilbert transform in $\KOp^{d-1}$ that arises when
$d$ is even, we see that when $d$ is even, $\KOp^{d-1}$ is a \emph{global
operator}. When applied to sufficiently nice functions, we have the equality
\[
  c_d (-\Delta)^{\frac{d-1}{2}} \RadonOp^* = \RadonOp^* \KOp^{d-1}.
\]
We summarize this in the following theorem regarding the continuity and
invertibility of the Radon transform on $\Sch(\R^d)$.

\begin{theorem}[see, e.g., {\citet{LudwigRadon,HelgasonIntegralGeometry}}]
  \label{thm:Radon-inversion-Schwartz}
  The operator $\RadonOp$ continuously maps $\Sch(\R^d) \to \Sch(\cyl)$.
  Moreover,
  \[
    \RadonOp^* \KOp^{d-1} \RadonOp = c_d (-\Delta)^{\frac{d-1}{2}} \RadonOp^*
    \RadonOp =  c_d \RadonOp^* \RadonOp (-\Delta)^{\frac{d-1}{2}} = \Id
  \]
  on $\Sch(\R^d)$.
\end{theorem}

Just like the Fourier transform, the Radon transform also admits a kind of
Plancheral's theorem~\citep[Theorem~1.3]{LudwigRadon}. In particular, it states
that $\KOp^{\frac{d-1}{2}}\RadonOp: L^2(\R^d) \to L^2(\cyl)$ is an isometry,
where $\KOp^{\frac{d-1}{2}}$ is defined via the frequency response
\[
  \hat{K}^{\frac{d-1}{2}}(\omega) = \sqrt{c_d} \, \abs{\omega}^{\frac{d-1}{2}}.
\]
Indeed, given $f \in \Sch(\R^d)$ we have
\[
  \norm*{\KOp^{\frac{d-1}{2}}\RadonOp f}_{L^2}^2
  = \sq{\KOp^{\frac{d-1}{2}}\RadonOp f, \KOp^{\frac{d-1}{2}}\RadonOp f}
  = \ang{f, \RadonOp^*\KOp^{\frac{d-1}{2}}\RadonOp f}
  = \ang{f, f}
  = \norm*{f}_{L^2}^2.
\]
The operator $\KOp^{\frac{d-1}{2}}\RadonOp$ admits a unique extension
$\KOp^{\frac{d-1}{2}}\RadonOp: L^2(\R^d) \to L^2(\cyl)$ since $L^2(\R^d) =
\cl{(\Sch(\R^d), \norm{\dummy}_{L^2})}$. We refer to this operator as the
\emph{half-filtered projection operator}. Moreover, this operator is invertible
on $L^2_\even(\cyl)$, the subspace of even functions in $L^2(\cyl)$, by its
adjoint operator. Indeed, we have that $\KOp^{\frac{d-1}{2}}\RadonOp: L^2(\R^d)
\to L^2_\even(\cyl)$ with inverse given by $\RadonOp^* \KOp^{\frac{d-1}{2}}:
L^2_\even(\cyl) \to L^2(\R^d)$.

Unlike the Fourier transform, extending the Radon transform to (tempered)
distributions is a delicate matter. Indeed, the na\"ive approach to define the
operators $\RadonOp$, $\KOp^{d-1}\RadonOp$, and $\RadonOp^*$ would be via
duality in a similar manner used to extend the Fourier transform to
$\Sch'(\R^d)$. This is summarized in the following definition.

\begin{definition}[{\citet[Definition~4]{UnserRadon}}] \label[defn]{defn:formal-Radon}
  The distribution $g \in \Sch'(\cyl)$ is the \emph{formal Radon transform} (or
  \emph{formal projection}) of the distribution $f \in \Sch'(\cyl)$ if
  \begin{equation}
    \sq{g, \psi} = \ang{f, \DualRadon{\psi}},
    \label{eq:formal-projection}
  \end{equation}
  for all $\psi \in \KOp^{d-1} \RadonOp\paren{\Sch(\R^d)}$. Likewise, $g \in
  \Sch'(\cyl)$ is a \emph{formal filtered projection} of $f \in \Sch'(\R^d)$ if
  \begin{equation}
    \sq{g, \psi} = \ang{f, \RadonOp^* \KOp^{d-1} \curly{\psi}},
    \label{eq:formal-filtered-projection}
  \end{equation}
  for all $\psi \in \RadonOp\paren{\Sch(\R^d)}$. Finally, $f \in \Sch'(\R^d)$ is
  the \emph{backprojection} of $g \in \Sch'(\cyl)$ if
  \begin{equation}
    \ang{f, \varphi} = \sq{g, \Radon{\varphi}},
    \label{eq:formal-backprojection}
  \end{equation}
  for all $\varphi \in \Sch(\R^d)$.
\end{definition}
The issue that arises with the formal definitions in \cref{defn:formal-Radon} is
that the definitions are not unique. In particular, for
\cref{eq:formal-projection,eq:formal-filtered-projection}, there are infinitely
many distributions $g \in \Sch'(\cyl)$ that satisfy \cref{eq:formal-projection}
or \cref{eq:formal-filtered-projection}. On the other hand, the definition in
\cref{eq:formal-backprojection} does provide a unique definition and therefore
we \emph{did not} refer to $f$ as the formal backprojection of $g$. We refer the
reader to~\citet{UnserRadon} for more details. The fundamental issue boils down
to the fact that the null space of the operator $\RadonOp^*: \Sch'(\cyl) \to
\Sch'(\R^d)$ contains many exotic functions~\citep[Theorem~4.2]{LudwigRadon}. To
this end, we can understand the Radon transform and related operators for a
variety of distributions by working with the so-called \emph{Radon-compatible
Banach spaces} of \citet{UnserRadon}.

Let $(\X, \norm{\dummy}_\X)$ be a Banach space such that $\Sch(\cyl)
\overset{\mathclap{\text{d.}}}{\hookrightarrow} \X
\overset{\mathclap{\text{d.}}}{\hookrightarrow} \Sch'(\cyl)$, where
$\overset{\mathclap{\text{d.}}}{\hookrightarrow}$ denotes a dense embedding.
Then, define the dual pair of Radon-compatible Banach spaces as $(\XRad
\coloneqq \cl{(\SchRad, \norm{\dummy}_\X)}, \XRad')$. This dual pair
satisfies many important properties, which we summarize in the following
proposition.

\begin{proposition}[{\citet[Theorem~7]{UnserRadon}}]
  \label[prop]{prop:Radon-compatible-spaces}
  If there exists a complementary Banach space $\XRad^\comp$ such
  that $\X = \XRad \oplus \XRad^\comp$, then
  \begin{enumerate}[leftmargin=*]
    \item The dual space is decomposable as $\X' = \XRad' \oplus
      (\XRad^\comp)'$.
    \item The complement space $\XRad^\comp$ is the null space of
      $\RadonOp^*\KOp^{d-1}: \X \to \RadonOp^* \KOp^{d-1}\paren{\XRad} \eqqcolon
      \Y$.
    \item The dual complement space $(\XRad^\comp)'$ is the null space of
      $\RadonOp^*: \X' \to \RadonOp^*\paren{\XRad'} = \Y'$.
    \item $\PRad \coloneqq \RadonOp\RadonOp^*\KOp^{d-1}: \X \to \XRad$ and
      $\PRad^* = \KOp^{d-1}\RadonOp\RadonOp^*: \X' \to \XRad'$ form a dual pair
      of projectors with $\PRad\paren{\X} = \XRad$ and $\PRad^*\paren{\X'} =
      \XRad'$.
  \end{enumerate}
\end{proposition}

This proposition allows us to have a unique definition of the Radon transform of
functions that live in the Banach space $\X'$ as above. Of particular interest
is the distributional definition of the Radon transform ridge distributions.
\begin{theorem}[{\citet[Proposition~10 \& Corollary~11]{UnserRadon}}]
  \label{thm:Radon-ridge-distribution}
  Let $(\vec{\alpha}_0, t_0) \in \cyl$ and let $r \in \Sch'(\R)$. Define the
  ridge distribution
  \[
    r_{(\vec{\alpha}_0, t_0)}(\vec{x}) \coloneqq r(\vec{\alpha}_0^\T\vec{x} -
    t_0).
  \]
  Furthermore, suppose that $\delta(\dummy -
  \vec{\alpha}_0) \, r(\dummy - t_0) \in \X'$, where $(\X, \X')$ is a dual pair
  of Banach spaces as above. Then,
  \begin{align*}
    \KOp^{d-1}\Radon{r_{(\vec{\alpha}_0, t_0)}} &=
    \PRad^*\curly{\delta(\dummy - \vec{\alpha}_0) \, r(\dummy - t_0)} \\
    \Radon{r_{(\vec{\alpha}_0, t_0)}} &=
    \PRad^*\curly{\delta(\dummy - \vec{\alpha}_0) \, (q_{d-1} * r)(\dummy - t_0)},
  \end{align*}
  where $q_{d-1}(t) = c_d \InvFourier[\omega]{1 / \abs{\omega}^{d-1}}(t)$ is the
  univariate impulse response of the Radon domain inverse filtering operator
  $(\KOp^{d-1})^{-1}$. In particular, when $\XRad = \X_\even$, the subspace of
  even functions in $\X$, then $\PRad^* = \P_\even$, the even
  projector, defined as
  \[
    \P_\even\curly{f} \coloneqq \frac{f + f^\vee}{2},
  \]
  where $f^\vee(\vec{z}) = f(-\vec{z})$ is the reflection of $f$ and $\vec{z}
  = (\vec{\alpha}, t) \in \cyl$.
\end{theorem}

Another important result about the Radon transform is regarding its connection
with the Fourier transform via the so-called \emph{Fourier slice theorem}. In
particular, for any $f \in \Sch'(\R^d)$, the Fourier slice theorem states that
\[
  \hat{\Radon{f}}(\vec{\alpha}, \omega) =
  \hat{f}(\omega \vec{\alpha}),
\]
where the Fourier transform on the left-hand side is the univariate Fourier
transform with respect to $t \to \omega$ and the Fourier transform on the
right-hand side is the usual multivariate Fourier transform of $f$. We refer the
reader to \citet[Chapter~10]{RammRadon} for the version of the Fourier slice
theorem that applies to $f \in \Sch'(\R^d)$.


