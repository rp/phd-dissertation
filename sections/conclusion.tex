\chapter{Concluding Remarks} \label{chap:conclusion}
Inspired by tools from spline theory we proposed and
studied a new family of Banach spaces, referred to as Radon-domain bounded
variation spaces. These spaces are intrinsically related to neural networks with
various activation functions including the popular ReLU. Additionally, the
results of this dissertation also provide compelling evidence that neural
networks can (and should!) be viewed as a type of spline.

In particular, we derived representer theorems over these spaces, showing that
the solution sets to data-fitting variational problems over these spaces are
completely characterized by functions that are realizable by neural networks.
Moreover, these variational problems can be recast as finite-dimensional neural
network training problems with regularization schemes related to weight decay
and path-norm regularization, providing new theoretical explanation for these
regularization schemes as well as providing several new, principled
regularization schemes for deep neural networks.

We also showed that on a bounded domain, these spaces are equivalent (as Banach
spaces) to the variation spaces of neural networks. This allowed us to study the
approximation properties of these Banach spaces, showing that these spaces are
immune to the curse of dimensionality. Using these approximation properties, we
were able to show that neural network estimators are near-minimax optimal
estimators for functions from these spaces.

\section{How Theory Informs Practice}
The variational framework developed in this dissertation informs the practical
use of neural networks.
\begin{itemize}
  \item By showing that the solutions to neural network training problems are
    solutions to variational problems over the Radon-domain $\BV$ space
    $\RBV^k$, we now have a concrete framework for comparing neural networks to
    more classical data-fitting techniques such as kernel methods (which are
    optimal solutions to variational problems over an RKHS) by comparing
    $\RBV^k$ to the RKHS of the particular kernel method.

  \item Many theoretical results regarding neural networks hold for
    infinite-width neural networks \citep{JacotNTK,WeiRegularization}. The
    representer theorems in \cref{chap:representer} show that it suffices to
    only consider (deep) neural networks of finite-width so long as the width is
    sufficiently wide.

  \item Skip connections are a common architectural choice in neural networks
    \citep{HeSkipConnections}. Many of the reasons for considering skip
    connections are based on heuristics. Skip connections are a natural
    by-product of the variational framework developed in this dissertation,
    providing a principled reason for considering skip connections in neural
    network architectures.

  \item It has become folklore in the machine learning community that deep
    neural networks are simply linear/kernel methods \citep{MonroeDeep}. The
    results of this dissertation show that neural networks learn functions in
    the (non-Hilbertian) $\RBV^k$ Banach spaces.
\end{itemize}

\section{Open Problems}
There are a number of open problems that remain regarding these new function
spaces. In the remainder of this chapter we outline several directions for
future work.

\subsection{Approximate Atomic Decomposition of
\texorpdfstring{$\RBV^k(\Omega)$}{RBVk(Omega)}}
In the univariate case, the Radon-domain $\BV$ space $\RBV^k$, reduces to
the classical $\BV^k$ space. On a bounded domain, it is well-known \citep[see,
e.g.,][]{PeetreBesov} that we have the continuous embeddings
\begin{equation}
  B^k_{1,1}[0,1] \cembed \BV^k[0,1] \cembed B^k_{1,\infty}[0,1].
  \label{eq:univariate-embeddings}
\end{equation}
Since Besov spaces admit atomic decompositions via wavelets
\citep{TriebelWaveletsDomains}, this result implies that the $\BV^k[0,1]$ spaces
approximately admit atomic decompositions since they are tightly sandwiched
between two very similar Besov spaces which do admit atomic decompositions, even
though the $\BV^k[0,1]$ spaces do not have unconditional bases.  It remains an
open question whether or not when $d \geq 1$ the $\RBV^k(\Omega)$ spaces admit
an approximate atomic decomposition, where $\Omega \subset \R^d$ is a bounded
domain.

Using classical function spaces, we cannot tightly sandwich $\RBV^k(\Omega)$
between two similar spaces. Indeed, using $L^2$-Sobolev spaces, we have from
\cref{subsec:barron-sobolev} the following sandwiching of $\RBV^k(\Omega)$
\[
  H^{d/2 + k + \varepsilon}(\Omega) \cembed \RBV^k(\Omega) \cembed
  H^{k-1}(\Omega),
\]
where $\varepsilon > 0$. The gap between the two Sobolev spaces implies that
classical function spaces are, perhaps, too coarse to tightly sandwich the new,
not classical $\RBV^k(\Omega)$ spaces. To this end, in the remainder of this
chapter, we propose a new scale of Banach spaces, which we refer to as
Radon-domain Besov spaces, and conjecture how the $\RBV^k(\Omega)$ spaces are
related to these new spaces as well as outline some technical difficulties in
actually trying to prove the conjecture.

\subsubsection{Radon-Domain Besov Spaces}
From the $L^2$-isometries of the Radon transform (see \cref{sec:transforms}),
consider the following $4$ parameter ($r, s \geq 0$, $1 \leq p < \infty$, $1
\leq q \leq \infty$)
family of function spaces
\begin{equation}
   \RB^{r, s}_{p, q}(\R^d) \coloneqq
   \RadonOp^*\KOp^{\frac{d-1}{2}}\paren{B^r_{p, q}(\Sph^{d-1})
   \otimes_{\alpha} B^s_{p, q}(\R)},
   \label{eq:RB-space}
\end{equation}
where $B^r_{p, q}(\Sph^{d-1})$ and $B^s_{p, q}(\R)$ are the usual Besov spaces
on $\Sph^{d-1}$ and $\R$, respectively, and $\otimes_{\alpha}$ denotes the
completion of the algebraic tensor product with respect to an appropriate tensor
norm $\alpha$ which we define explicitly in \cref{eq:tensor-norm}. We refer to $\RB^{r,
s}_{p, q}(\R^d)$ as a \emph{Radon-domain Besov space} since it is the tensor
product of Besov spaces in the (half-filtered) Radon domain. Note that since
$\RadonOp^*\KOp^{\frac{d-1}{2}}$ is an $L^2$-isometry combined with the
intertwining of Laplacians and the Radon transform, we have that
\[
    \RB^{0, s}_{2, 2}(\R^d) = H^s(\R^d),
\]
where $H^s(\R^d)$ is the usual $s$th-order $L^2$-Sobolev space on $\R^d$.

This definition captures a kind of anisotropy between the direction and offset
variables of the Radon domain. In order to define the tensor norm $\alpha$, we
use the fact that Besov spaces admit atomic decompositions via sequence
space representations.

From~\citet{NarcowichNeedlets,NarcowichBesovNeedlet}, there exist localized
(i.e., wavelet-like) frames on the sphere $\Sph^{d-1}$. These frames are
referred to as \emph{needlets} due to their almost exponential localization and
that they look like needles on the sphere.  Let
$\curly{\varphi_\vec{\eta}}_{\vec{\eta} \in \X}$, denote the needlet system.
The index set $\X \subset \Sph^{d-1}$ is a countable collection of the centers
of each needlet function $\varphi_{\vec{\eta}}$. We can decompose the index set
as $\X = \bigcup_{j=0}^\infty \X_j$, where the $\X_j$ indexes all needlets at
scale $j$. This system forms a Parseval frame for $L^2(\Sph^{d-1})$ (i.e., a
frame with frame bounds equal to $1$)~\cite[Theorem~5.2]{NarcowichNeedlets}.
Also, let
$\curly{\psi_{j, k}}_{j \in \N_0, k \in \Z}$ denote the inhomogeneous Meyer
wavelet system \citep{LemarieOndelettes}, where
\[
\psi_{0, k}(x) \coloneqq \phi(x - k), \quad k \in \Z,
\]
where $\phi$ is the Meyer scaling function and
\[
\psi_{j+1, k}(x) \coloneqq 2^{j/2} \psi(2^j x - k), \quad j \in \N_0, k \in \Z,
\]
where $\psi$ is the Meyer wavelet function. The system $\curly{\psi_{j, k}}_{j
\in \N_0, k \in \Z}$ forms an orthobasis for $L^2(\R)$.

Next, it is well-known that \citep[see,
e.g.,][Theorem~5.5]{NarcowichBesovNeedlet} that given $f \in
\Sch'(\Sph^{d-1})$\footnote{$\Sch'(\Sph^{d-1})$ is the space of distributions on
the sphere, which is the continuous dual of the space of test functions on the
sphere $\Sch(\Sph^{d-1}) \coloneqq C^\infty(\Sph^{d-1})$.}, $f \in
B^r_{p,q}(\Sph^{d-1})$ if and only if
\[
    \norm{f}_{B^r_{p,q}} = \paren{\sum_{m=0}^\infty \paren{2^{m(r + (d - 1)/2 - (d - 1)/p)} \paren{\sum_{\vec{\eta} \in \X_m} \abs{\ang{g, \varphi_\vec{\eta}}}^p}^{1/p}}^q}^{1/q} < \infty,
\]
where $\curly{\varphi_{\vec{\eta}}}_{\vec{\eta} \in \X_m}$ are the needlets at
scale $m$, with appropriate modification when $q = \infty$. It is also well-known that \citep[see,
e.g.,][Chapter~6]{MeyerWaveletsOperators} given $f \in \Sch'(\R)$, $f
\in B^s_{p,q}(\R)$ if and only if
\[
    \norm{f}_{B^s_{p,q}} =
    \paren{\sum_{j=0}^\infty \paren{2^{j(s + 1/2 - 1/p)} \paren{\sum_{k \in \Z}
    \abs{\ang{f, \psi_{j, k}}}^p}^{1/p}}^q}^{1/q} < \infty,
\]
with appropriate modification when $q = \infty$.  Note that the pairings
$\ang{\dummy, \dummy}$ that appear in the above two displays are well-defined
since $\varphi_\vec{\eta} \in \Sch(\Sph^{d-1})$ and $\psi_{j, k} \in \Sch(\R)$.
From these two atomic decompositions, we define norm on $B^{r}_{p,
q}(\Sph^{d-1}) \otimes_\alpha B^{s}_{p, q}(\R)$ as
\begin{equation}
  \begin{aligned}
    &\norm{g}_{B^{r}_{p, q}(\Sph^{d-1}) \otimes B^{s}_{p, q}(\R)} \\
    &=
    \paren{\sum_{m=0}^\infty \sum_{j=0}^\infty \paren{2^{m(r +
    (d-1)/2 - (d-1)/p) + j(s + 1/2 - 1/p)} \paren{\sum_{\vec{\eta} \in \X_{m}}
    \sum_{k \in \Z} \abs{\sq*[\Big]{g, \varphi_\vec{\eta} \otimes \psi_{j,
    k}}}^p}^{1/p}}^q}^{1/q}.
  \end{aligned}
  \label{eq:tensor-norm}
\end{equation}
This is the tensor norm used to complete the tensor product in
\cref{eq:RB-space}. One can check that the system $\curly{\rho_{j, k,
\vec{\eta}}}_{j \in \N_0, k \in \Z, \vec{\eta} \in \X}$ defined by
\[
  \rho_{j, k, \vec{\eta}}(\vec{x}) =\int_{\Sph^{d-1}} \tilde{\psi}_{j,
  k}(\vec{\alpha}^\T\vec{x}) \varphi_\vec{\eta}(\vec{\alpha})
  \dd\sigma(\vec{\alpha}),
\]
is a Parseval frame for $L^2(\R^d)$, where $\tilde{\psi}_{j, k} =
\KOp^{\frac{d-1}{2}}\psi_{j, k}$, i.e.,
\[
  \reallywidehat{\KOp^{\frac{d-1}{2}}\psi_{j, k}}(\omega) =
  \hat{K}^{\frac{d-1}{2}}(\omega) \hat{\psi}_{j, k}(\omega) = \sqrt{c_d}
  \abs{\omega}^{\frac{d-1}{2}} \hat{\psi}_{j, k}(\omega).
\]
This system has the property that given $f \in \Sch'(\R^d)$,
\[
  \ang{f, \rho_{j, k, \vec{\eta}}} = \sq{\KOp^{\frac{d-1}{2}}\RadonOp f,
  \varphi_\vec{\eta} \otimes \psi_{j, k}},
\]
and so we can equip the space $\RB^{r, s}_{p, q}(\R^d)$ with the norm
\[
  \norm{f}_{\RB^{r,s}_{p,q}} \coloneqq \paren{\sum_{m=0}^\infty \sum_{j=0}^\infty
  \paren{2^{m(s_1 + (d-1)/2 - (d-1)/p) + j(s_2 + 1/2 - 1/p)}
  \paren{\sum_{\vec{\eta} \in \X_{m}} \sum_{k \in \Z} \abs{\ang{f, \rho_{j, k,
  \vec{\eta}}}}^p}^{1/p}}^q}^{1/q}
\]
making it a Banach space, with appropriate modification when $q = \infty$.
\begin{remark}
    The frame elements $\rho_{j, k, \vec{\eta}}$ can be viewed as a weighted
    average of the (half-filtered) wavelet ridge functions
    \[
        \tilde{\psi}_{j, k}(\vec{\alpha}^\T\vec{x}),
    \]
    averaged over all directions $\vec{\alpha} \in \Sph^{d-1}$, where the
    weighting function is the needlet $\varphi_\vec{\eta}$, centered at the
    direction $\vec{\eta} \in \X \subset \Sph^{d-1}$. Therefore, these frame
    elements are localized functions, parameterized by a location $j$, scale
    $k$, and direction $\vec{\eta}$. We consider this a localized ridgelet-type
    tight frame.
\end{remark}
Let $\Omega \subset \R^d$ be a bounded domain with a sufficiently nice boundary.
We can define the space $\RB^{r, s}_{p, q}(\Omega)$ as
\[
    \RB^{r, s}_{p, q}(\Omega) \coloneqq \curly{f: \mathcal{D}'(\Omega) \st \exists g \in
    \RB^{r, s}_{p, q}(\R^d) \:\: \subj \:\: \eval{g}_\Omega = f},
\]
This is a Banach space when equipped with the norm
\[
    \norm{f}_{\RB^{r, s}_{p, q}(\Omega)} \coloneqq \inf_{g \in \RB^{r, s}_{p,
    q}(\R^d)} \, \norm{g}_{\RB^{r, s}_{p, q}(\R^d)} \quad\subj\quad
    \eval{g}_\Omega = f.
\]
Moreover, these spaces admit atomic decompostions since the Besov spaces used to
define $\RB^{r,s}_{p,q}(\R^d)$ admit atomic decompositions.  From the univariate
embeddings in \cref{eq:univariate-embeddings}, we conjecture the following
embeddings for the $\RBV^k(\Omega)$ spaces.
\begin{conjecture}
  Let $\Omega \subset \R^d$ be a bounded domain. We conjecture that we have the
  following continuous embeddings
  \[
    \RB^{0, k + \frac{d-1}{2}}_{1,1}(\Omega) \cembed \RBV^k(\R^d)
    \cembed \RB^{0, k + \frac{d-1}{2}}_{1, \infty}(\Omega)
  \]
  when $\Omega$ has a sufficiently nice boundary.
\end{conjecture}
The main technical difficulties in proving this conjecture arise in the
construction of a nice extension operator from $\RB^{r,s}_{p,q}(\Omega) \to
\RB^{r,s}_{p,q}(\R^d)$. This problem is very similar to finding an intrinsic
definition\footnote{Our current definition hinges on the Radon transform, which
is a global operator.} of $\RBV^k$ on a bounded domain. In the case of Besov
spaces, such extension operators exist due to their intrinsic definition via
moduli of continuity\footnote{The modulus of continuity is a local quantity.}
over the more standard Littlewood--Paley characterization studied in harmonic
analysis \citep{DeVoreBesovDomain}.

\subsection{Generalized Radon Transforms and New Representer Theorems}
In the representer theorems in
\cref{thm:RBVs-rep-thm,cor:shallow-representer,thm:rep-thm-domain}, the
resulting atoms have normalized singularities along hyperplanes in the sense of
\citet[Definition~5]{DonohoUnconditional}. The singularities are along
hyperplanes due to the Radon transform, since the Radon transform of a function
is computed via its integral along hyperplanes.

A natural followup question is about defining function spaces via integral
transforms along other low-dimensional manifolds, resulting in representer
theorems with atoms that have different kinds of normalized singularities. We
believe that this should be possible through the framework of \emph{generalized
Radon transforms} \citep{QuintoGRT}. In particular, we saw in \cref{eq:RadjRinv}
that the standard Radon transform $\RadonOp$ satisfies
\[
  \paren{\RadonOp^*\RadonOp}^{-1} = c_d (-\Delta)^{\frac{d-1}{2}}.
\]
We believe this problem can be solved by considering generalized Radon
transforms $\GRadonOp$ such that $\paren{\GRadonOp^*\GRadonOp}^{-1}$ is a
pseudodifferential operator, in which case there are some generic results about
the inversion of the transform and its dual \citep{QuintoGRT}. The main
technical difficulty that arises in proving such a result revolves around
developing a distributional theory of these transforms.

\subsection{Alternative Banach Spaces for Vector-Valued Functions}
In \cref{rem:RBV-M-isomorphism}, we saw that the space $\RBV^k(\R^d)$ is
isometrically isomorphic to $\M_k(\cyl) \times \mathcal{P}_{k-1}(\R^d)$. When
defined the vector-valued versions of these spaces (in the case that $k = 2$),
our definition of $\RBV^2(\R^d; \R^D)$ from \cref{eq:vv-RBV2,eq:vv-RBV2-norm} is
isometrically isomorphic to the space $\ell^1([D]; \M_\even(\cyl)) \times
\mathcal{P}_1(\R^d; \R^D)$, where $\mathcal{P}_1(\R^d; \R^D)$ denotes the
$D$-fold Cartesian product of $\mathcal{P}_1(\R^d)$. The main limitation of this
construction is that the resulting shallow neural networks in the vector-valued
representer theorem in \cref{lemma:vv-banach-rep-thm} essentially correspond to
$D$ separate, decoupled scalar-output neural networks, as opposed to a
vector-valued neural network with shared neurons.

Suppose $k = 2$, a natural question to ask is the existence of an analytic
characterization of a Banach space isometrically isomorphic to $\M_\even(\cyl;
\ell^p([D])) \times \mathcal{P}_1(\R^d; \R^D)$, for some $1 < p < \infty$.
The space $\M_\even(\cyl; \ell^p([D]))$ is likely to promote coupling between
the vector-valued outputs as opposed to the space $\ell^1([D]; \M_\even(\cyl))$.

